function path() {
    var response = "";
    for (let part of arguments) {
        if (response.length > 0 && response[response.length-1] != "/") {
            response += "/";
        }
        response += part;
    }
    return response;
}

function ucfirst(text) {
    return text[0].toUpperCase()+text.substr(1);
}

module.exports = {
    path,
    ucfirst
};