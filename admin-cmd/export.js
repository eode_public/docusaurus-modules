/***
Create a zip and export on server
***/

const fs = require("fs");
const zip = require("node-zip-dir");
var request = require("request");
const config = require("./config");
const com = require("./common");

function start() {
    process.stdout.write("\n\nExport\n");

    // create zip
    console.log("Creating zip file...");
    zip.zip(com.path(config.workDir, config.buildDir), "tmp_export.zip").then(function() {
        console.log("Zip created\n");

        // call import on server
        console.log("Exporting builded website...");

        var stopLoading = false;

        request.post({
                url: config.websiteExportUrl,
                formData: {
                    password: config.password,
                    tmpzip: fs.createReadStream("tmp_export.zip")
                }
            },
            (err, httpResponse, body) => {
                stopLoading = body;

                fs.unlinkSync("tmp_export.zip");
            }
        );

        process.stdout.write("Uploading.");

        var int;
        int = setInterval(() => {
            if (stopLoading) {
                process.stdout.write(stopLoading);
                clearInterval(int);
            }
            else {
                process.stdout.write(".");
            }
        }, 500);
    }).catch(function(err) {
        process.stdout.write(" ERROR\n");
        console.error(err);
    });
}

start();