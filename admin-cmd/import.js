/***
Create a server to update website
***/

const fs = require("fs");
const http = require("http");
const formidable = require("formidable");
const zip = require("node-zip-dir");

const config = require("./config");
const com = require("./common");

http.createServer(function(req, res) {
    if (req.url == "/" && req.method.toLowerCase() == "post") {
        var form = new formidable.IncomingForm();

        console.log("Start receiving");

        form
        .on("error", err => {
            console.error(err);
        })
        .on("aborted", () => {
            res.writeHead(200, {"content-type": "text/plain"});
            res.end(" ERROR\n");
        })
        .parse(req, (err, fields, files) => {
            if (!fields.hasOwnProperty("password") || fields.password != config.password) {
                console.error("bad password");
                res.writeHead(200, {"content-type": "text/plain"});
                res.end(" ERROR\nBad password");
                return;
            }

            if (files.hasOwnProperty("tmpzip")) {
                var file = files.tmpzip;
                console.log("begin unzip " + file.path);
                zip.unzip(file.path, "../docusaurus").then(function() {
                    console.log("success");
                    res.writeHead(200, {"content-type": "text/plain"});
                    res.end(" OK\nWebsite updated !");
                    return;
                }).catch(function(err) {
                    console.error(err);
                    res.writeHead(200, {"content-type": "text/plain"});
                    res.end(" ERROR\n");
                    return;
                });
            }
            else {
                console.log("file not found");
                res.writeHead(200, {"content-type": "text/plain"});
                res.end(" ERROR\n");
                return;
            }
        });

        return;
    }

    // show a file upload form
    res.writeHead(200, {'content-type': 'text/html'});
    res.end(
        "This was a triumph!<br/>I'm making a note here:<br/>Huge success!<br/><br/>It's hard to overstate<br/>my satisfaction.<br/><br/>Aperture Science:<br/>We do what we must<br/>because we can<br/>For the good of all of us.<br/>Except the ones who are dead.<br/><br/>But there's no sense crying<br/>over every mistake.<br/>You just keep on trying<br/>'til you run out of cake.<br/>And the science gets done.<br/>And you make a neat gun<br/>for the people who are<br/>still alive.<br/><br/>I'm not even angry...<br/>I'm being so sincere right now.<br/>Even though you broke my heart,<br/>and killed me.<br/><br/>And tore me to pieces.<br/>And threw every piece into a fire.<br/>As they burned it hurt because<br/>I was so happy for you!<br/><br/>Now, these points of data<br/>make a beautiful line.<br/>And we're out of beta.<br/>We're releasing on time!<br/>So I'm GLaD I got burned!<br/>Think of all the things we learned!<br/>for the people who are<br/>still alive.<br/><br/>Go ahead and leave me...<br/>I think I'd prefer to stay inside...<br/>Maybe you'll find someone else<br/>to help you.<br/>Maybe Black Mesa?<br/>That was a joke. Ha Ha. Fat Chance!<br/><br/>Anyway this cake is great!<br/>It's so delicious and moist!<br/><br/>Look at me: still talking<br/>when there's science to do!<br/>When I look out there,<br/>it makes me glad I'm not you.<br/><br/>I've experiments to run.<br/>There is research to be done.<br/>On the people who are<br/>still alive.<br/>And believe me I am<br/>still alive.<br/>I'm doing science and I'm<br/>still alive.<br/>I feel fantastic and I'm<br/>still alive.<br/>While you're dying I'll be<br/>still alive.<br/>And when you're dead I will be<br/>still alive<br/><br/>Still alive.<br/><br/>Still alive."
    );
}).listen(4000);
