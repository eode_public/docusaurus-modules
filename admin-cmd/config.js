module.exports = {
    workDir: "..",
    docsPath: "docs",
    blogPath: "website/blog",
    staticPath: "website/static",
    siteConfig: "website/siteConfig.js",
    buildDir: "website/build/Wonderland",
    sidebars: "website/sidebars.json",
    modulesPath: "../all-modules",
    moduleSubPath: "Website",
    modules: [
        "AssetBundles",
        "ColorMatrix",
        "Common",
        "FSM",
        "GameEntity",
        "Inputs",
        "Modular",
        // "Net",
        "Prebuild"
    ],
    blog: {
        author: "EODE team",
        authorURL: "https://eode.studio/",
        authorFBID: "200000000000000"
    },
    websiteExportUrl: "http://docuimport.eode.studio",
    password: "azertyuiop^qsdfghjklmwxcvbn,"
};