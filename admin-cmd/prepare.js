/***
Prepares files
Gets all docs in modules
***/

const fs = require("fs-extra");
const config = require("./config");
const com = require("./common");

function start() {
    // remove olds docs
    var docPath = com.path(config.workDir, config.docsPath);
    for (let file of fs.readdirSync(docPath)) {
        if (file.startsWith(".")) continue; // ignore .gitkeep

        fs.unlinkSync(com.path(docPath, file));
    }

    // remove olds blog posts
    var blogPath = com.path(config.workDir, config.blogPath);
    for (let file of fs.readdirSync(blogPath)) {
        if (file.startsWith(".")) continue; // ignore .gitkeep

        fs.unlinkSync(com.path(blogPath, file));
    }

    // remove olds statics
    var staticPath = com.path(config.workDir, config.staticPath);
    for (let module of config.modules) {
        try {
            var smodpath = com.path(staticPath, module);
            fs.accessSync(smodpath);
            for (let f of fs.readdirSync(smodpath)) {
                fs.unlinkSync(com.path(smodpath, f));
            }
            rmdirSync(smodpath);
        } catch(e){}
    }

    // exports docs
    var defaultFileDoc = {};
    var sidebars = {};
    var sidebarsNb = 0;

    for (let module of config.modules) {
        const dpath = com.path(config.workDir, config.modulesPath, module, config.moduleSubPath, "docs");

        const files = fs.readdirSync(dpath);

        for (let file of files) {
            if (file.startsWith(".")) continue; // ignore .gitkeep

            if (file == "sidebar.json") {
                sidebars[module] = com.path(dpath, file);
                ++sidebarsNb;
                continue;
            }

            let fid = module.toLowerCase()+"-"+file.substr(0, file.length-3);

            // default file
            if (file.startsWith("_")) {
                defaultFileDoc[module] = fid;
            }

            fs.readFile(com.path(dpath, file), "utf-8", (err, data) => {
                if (err) {
                    console.log("[Doc] "+module+":"+file+" -> ERROR");
                    throw err;
                }

                let fdata = "---\nid: "+fid;
                fdata += data.substr(data.indexOf("\n"));

                fs.writeFileSync(com.path(config.workDir, config.docsPath, module+"_"+file), fdata);
                console.log("[Doc] "+module+":"+file+" -> success");
            });
        }
    }

    // exports blog
    for (let module of config.modules) {
        const bpath = com.path(config.workDir, config.modulesPath, module, config.moduleSubPath, "blog");

        const files = fs.readdirSync(bpath);

        for (let file of files) {
            if (file.startsWith(".")) continue; // ignore .gitkeep

            fs.readFile(com.path(bpath, file), "utf-8", (err, data) => {
                if (err) {
                    console.log("[Blog] "+module+":"+file+" -> ERROR");
                    throw err;
                }

                let fdata = "---\n";
                for (let k in config.blog) {
                    fdata += k+": "+config.blog[k]+"\n";
                }
                fdata += data.substr(data.indexOf("\n")+1, 7);
                fdata += "["+com.ucfirst(module)+"] ";
                fdata += data.substr(data.indexOf("\n")+8);

                fs.writeFileSync(com.path(config.workDir, config.blogPath, file.substr(0, file.length-3)+"_"+module+".md"), fdata);
                console.log("[Blog] "+module+":"+file+" -> success");
            });
        }
    }

    // exports statics
    for (let module of config.modules) {
        try {
            fs.mkdirSync(com.path(staticPath, module));
        } catch(e){}

        const spath = com.path(config.workDir, config.modulesPath, module, config.moduleSubPath, "static");

        try {
            const files = fs.readdirSync(spath);

            for (let file of files) {
                fs.copy(com.path(spath, file), com.path(staticPath, module, file), err2 => {
                    if (err2) {
                        console.log("[Static] "+module+":"+file+" -> ERROR");
                        throw err2;
                    }

                    console.log("[Static] "+module+":"+file+" -> success");
                });
            }
        }
        catch(e) { }
    }

    // siteConfig
    fs.readFile(com.path(config.workDir, "admin-cmd/siteConfig.js"), "utf-8", (err, data) => {
        if (err) {
            console.log("Update siteConfig -> ERROR");
            throw err;
        }
        var moduleLinks = "";
        for (let module in defaultFileDoc) {
            moduleLinks += "    {doc: '"+defaultFileDoc[module]+"', label: '"+com.ucfirst(module)+"'},\n";
        }

        data = data.replace("__headerLinks__", moduleLinks);

        fs.writeFileSync(com.path(config.workDir, config.siteConfig), data);
        console.log("Update siteConfig -> success");
    });

    // sidebars
    var sidebarsContent = [];
    for (let module in sidebars) {
        fs.readFile(sidebars[module], "utf-8", (err, data) => {
            if (err) {
                console.log("Update sidebars -> ERROR");
                throw err;
            }

            sidebarsContent.push("\""+module+"\": "+
                (data
                    .split("[\"").join("[\""+module.toLowerCase()+"-")
                    .split(",\"").join(",\""+module.toLowerCase()+"-")
                    .split(", \"").join(",\""+module.toLowerCase()+"-")
                )
            );

            --sidebarsNb;
        });
    }

    var idInterval;
    idInterval = setInterval(() => {
        if (sidebarsNb == 0) {
            clearInterval(idInterval);

            sidebarsContent = "{\n"+sidebarsContent.join(",\n")+"\n}";

            fs.writeFileSync(com.path(config.workDir, config.sidebars), sidebarsContent);
            console.log("Update sidebars -> success");
        }
    }, 50)
};

start();