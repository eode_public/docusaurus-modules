## Requirements
- Node v8.5.0 (copyFile)
- Created with docusaurus 1.0.13

## Install
### Local
```bash
# docusaurus install
cd website
npm i

# admin install (this launch prepare script... just ignore)
cd ../admin-cmd
npm i
```

### Server
Add the cmd-admin folder only.

It is not necessary to update server files if you change the module list for example. The export script exports the entire generated website.

_Example to configure your server_
```bash
upstream docuimport.eode.studio {
    server localhost:4000;
}

server {
    listen 80;
    listen [::]:80;
    server_name docuimport.eode.studio;

    client_max_body_size 15M;

    location / {
        proxy_pass http://docuimport.eode.studio;
    }

    location /websocket/ {
        proxy_pass http://docuimport.eode.studio;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
}

server {
    listen 80;
    listen [::]:80;
    server_name wonderland.eode.studio;

    location / {
        root /home/wonderland/docusaurus;
    }
}
```

_Example to start the import.js script_
```js
pm2 start import.js
```

## config
Use *admin-cmd/config.js* to config the deploy module
```js
module.exports = {
    workDir: "..", // work directory for this script (added to all others path)
    docsPath: "docs", // docusaurus docs dir
    blogPath: "website/blog", // docusaurus blog dir
    staticPath: "website/static", // docusaurus static dir
    siteConfig: "website/siteConfig.js", // docusaurus siteConfig file location
    buildDir: "website/build/Wonderland", // docusaurus build path
    sidebars: "website/sidebars.json", // docusaurus sitebars file location
    modulesPath: "..", // where is modules
    moduleSubPath: "Website", // sub dir in module contains docs, blog and static folders
    modules: [ // module list to add
        "Common",
        "AssetBundlesManager"
    ],
    blog: { // blog user configuration
        author: "EODE team",
        authorURL: "https://eode.studio/",
        authorFBID: "200000000000000" // FB page id
    },
    websiteExportUrl: "http://docuimport.eode.studio", // import script server address
    password: "&jhdd5215425&54sdf21f" // export connection password
};
```

Use *admin-cmd/siteConfig.js* to configure docusaurus.


## Commands
```bash
# prepare and export
cd admin-cmd
npm run deploy

# steps
# prepare docs etc
npm run prepare

# build docusaurus
npm run build

# export build on server
npm run export

#other
# start docusaurus (local)
npm run start
```

## docs structure
- Module_Name
    - Website
        - blog (contains news to append in news page)
            - 2017-05-24-news-name.md
            - year-month-day-name.md
        - docs (contains all docs for a module)
            - my-doc-1.md
            - my-doc-2.md
            - sidebar.json (contains the sidebar, order of pages..)
        - static (contains all static files)
            - my-image.jgp
            - troll-gif.gif
- Module_2
    - Website
        - ...
        - ...


## blog post basic
```md
---
title: My post title
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies.

<!--truncate-->

Mauris vestibulum ullamcorper nibh, ut semper purus pulvinar ut. Donec volutpat orci sit amet mauris malesuada, non pulvinar augue aliquam. Vestibulum ultricies at urna ut suscipit. Morbi iaculis, erat at imperdiet semper, ipsum nulla sodales erat, eget tincidunt justo dui quis justo. Pellentesque dictum bibendum diam at aliquet. Sed pulvinar, dolor quis finibus ornare, eros odio facilisis erat, eu rhoncus nunc dui sed ex. Nunc gravida dui massa, sed ornare arcu tincidunt sit amet. Maecenas efficitur sapien neque, a laoreet libero feugiat ut.
```

## doc basic
```md
---
title: My doc title
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies.
```

## sidebar basic
```json
{
  "First Category": ["docfile1", "docfile2"],
  "Second Category": ["docfile3", "docfile4", "docfile5"]
}
```